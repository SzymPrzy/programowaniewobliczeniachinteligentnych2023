from Lab1.lab1 import *


# b. płaskiej pionowej powierzchni o ograniczonej szerokości i wysokości

def generate_surface_vertical(start: Point, width: float, height: float, angle: float, number_of_points: int):
    points = generate_rectangular_random_point_cloud(start.x, start.x + width, start.y, start.y, start.z,
                                                     start.z + height, number_of_points)
    if angle != 0:
        points_rotated = []

        for p in points:
            points_rotated.append(rotate_point_z(p, start, angle))

        return points_rotated

    else:
        return points


if __name__ == '__main__':
    s = 10
    reference_points = [Point(s, s, s), Point(-s, -s, -s), Point(s, s, -s), Point(s, -s, s), Point(-s, s, s),
                        Point(-s, -s, s), Point(s, -s, -s), Point(-s, s, -s)]

    surface_0 = generate_surface_vertical(Point(0, 0, 0), 10, 5, 0, 1000)
    surface_1 = generate_surface_vertical(Point(0, 0, 5), 10, 5, math.pi, 1000)
    surface_2 = generate_surface_vertical(Point(0, 5, 0), 10, 5, math.pi * 1.5, 1000)
    surface_3 = generate_surface_vertical(Point(5, 0, 0), 10, 5, math.pi * 2.5, 1000)

    ax = plt.axes(projection='3d')
    graph_point_array(ax, reference_points)
    graph_point_array(ax, surface_0)
    graph_point_array(ax, surface_1)
    graph_point_array(ax, surface_2)
    graph_point_array(ax, surface_3)

    plt.show()

    save_points_to_csv("vertical_surfaces.xyz", surface_0)

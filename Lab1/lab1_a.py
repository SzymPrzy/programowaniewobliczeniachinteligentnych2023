from Lab1.lab1 import *


# a. płaskiej poziomej powierzchni o ograniczonej szerokości i długości

def generate_surface_horizontal(start: Point, width: float, height: float, number_of_points: int):
    return generate_rectangular_random_point_cloud(start.x, start.x + width, start.y, start.y + height, start.z,
                                                   start.z, number_of_points)


if __name__ == '__main__':
    s = 10
    reference_points = [Point(s, s, s), Point(-s, -s, -s), Point(s, s, -s), Point(s, -s, s), Point(-s, s, s),
                        Point(-s, -s, s), Point(s, -s, -s), Point(-s, s, -s)]

    surface_0 = generate_surface_horizontal(Point(-10, 0, 0), 10, 5, 1000)
    surface_1 = generate_surface_horizontal(Point(0, 6, 0), 10, 5, 1000)
    surface_2 = generate_surface_horizontal(Point(-0, 0, 7), 10, 5, 1000)

    ax = plt.axes(projection='3d')
    graph_point_array(ax, reference_points)
    graph_point_array(ax, surface_0)
    graph_point_array(ax, surface_1)
    graph_point_array(ax, surface_2)

    save_points_to_csv("horizontal_surfaces.xyz", surface_0)
    plt.show()

# This is a sample Python script.
import random

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# Napisać funkcje generujące chmury punktów położonych w przestrzeni 3D na:
# a. płaskiej poziomej powierzchni o ograniczonej szerokości i długości
# b. płaskiej pionowej powierzchni o ograniczonej szerokości i wysokości
# c. powierzchni cylindrycznej o zadanym promieniu i ograniczonej wysokości

import numpy as np
import matplotlib.pyplot as plt
import csv
import math


class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


def rotate_point_yaxis(point: Point, axis: Point, angle):
    # Convert angle to radians
    theta = math.radians(angle)

    # Normalize axis vector
    axis.x = axis.x / math.sqrt(axis.x ** 2 + axis.y ** 2 + axis.z ** 2)
    axis.y = axis.y / math.sqrt(axis.x ** 2 + axis.y ** 2 + axis.z ** 2)
    axis.z = axis.z / math.sqrt(axis.x ** 2 + axis.y ** 2 + axis.z ** 2)

    # Calculate rotation matrix components
    cos_theta = math.cos(theta)
    sin_theta = math.sin(theta)
    one_minus_cos = 1 - cos_theta

    ux = axis.x
    uy = axis.y
    uz = axis.z

    # Calculate rotation matrix
    rotation_matrix = [
        [cos_theta + ux ** 2 * one_minus_cos, ux * uy * one_minus_cos - uz * sin_theta,
         ux * uz * one_minus_cos + uy * sin_theta],
        [uy * ux * one_minus_cos + uz * sin_theta, cos_theta + uy ** 2 * one_minus_cos,
         uy * uz * one_minus_cos - ux * sin_theta],
        [uz * ux * one_minus_cos - uy * sin_theta, uz * uy * one_minus_cos + ux * sin_theta,
         cos_theta + uz ** 2 * one_minus_cos]
    ]

    # Apply rotation matrix to point
    rotated_point = Point(
        rotation_matrix[0][0] * point.x + rotation_matrix[0][1] * point.y + rotation_matrix[0][2] * point.z,
        rotation_matrix[1][0] * point.x + rotation_matrix[1][1] * point.y + rotation_matrix[1][2] * point.z,
        rotation_matrix[2][0] * point.x + rotation_matrix[2][1] * point.y + rotation_matrix[2][2] * point.z
    )

    return rotated_point


def rotate_x(point: Point, angle):
    # Convert angle to radians
    theta = np.radians(angle)

    # Define rotation matrix
    rotation_matrix = np.array([
        [1, 0, 0],
        [0, np.cos(theta), -np.sin(theta)],
        [0, np.sin(theta), np.cos(theta)]
    ])

    # Apply rotation matrix to point
    point_vector = np.array([point.x, point.y, point.z])
    rotated_point_vector = np.dot(rotation_matrix, point_vector)

    return Point(rotated_point_vector[0], rotated_point_vector[1], rotated_point_vector[2])


def rotate_point_cloud_x(point_cloud, angle):
    rotated_points = []
    for p in point_cloud:
        rotated_points.append(rotate_x(p,angle))
    return rotated_points

def generate_random_point_cloud2(x1, x2, y1, y2, z1, z2, number_of_points):
    points = []
    for _ in range(number_of_points):
        rand_x = random.uniform(x1, x2)
        rand_y = random.uniform(y1, y2)
        rand_z = random.uniform(z1, z2)

        points.append(Point(rand_x, rand_y, rand_z))

    return points


class Surface:
    def __init__(self, c1: Point, c2: Point, c3: Point):
        self.corner_1 = c1
        self.corner_2 = c2
        self.corner_3 = c3

    def generate_random_points_on_surface(self, number_of_points):
        points = []
        for _ in range(number_of_points):
            # Define the vertices of a triangular surface
            vertices = np.array([[self.corner_1.x, self.corner_2.x, self.corner_3.x],
                                 [self.corner_1.y, self.corner_2.y, self.corner_3.y],
                                 [self.corner_1.z, self.corner_2.z, self.corner_3.z]])

            # Select a random triangle from the surface
            triangle = vertices[random.sample(range(3), 3)]

            # Generate a random point inside the triangle
            u, v = sorted([random.random(), random.random()])
            w = 1 - u - v
            point = u * triangle[0] + v * triangle[1] + w * triangle[2]

            points.append(Point(point[0], point[1], point[2]))

        return points

    def graph(self):
        ax = plt.axes(projection='3d')

        xdata = [self.corner_1.x, self.corner_2.x, self.corner_3.x, self.corner_1.x]
        ydata = [self.corner_1.y, self.corner_2.y, self.corner_3.y, self.corner_1.y]
        zdata = [self.corner_1.z, self.corner_2.z, self.corner_3.z, self.corner_1.z]

        ax.plot3D(xdata, ydata, zdata)


# plaska pozioma powierzchnia
#  corner_y ------------ corner_xy
#    |                      |
#   height       XY         |
#    |                      |
#  start -------width----- corner_x
class SurfaceHorizontal:
    def __init__(self, start: Point, width, height):
        self.start = start
        self.width = width
        self.height = height

        self.corner_x = Point(start.x + width, start.y, start.z)
        self.corner_y = Point(start.x, start.y + height, start.z)
        self.corner_xy = Point(start.x + width, start.y + height, start.z)

    def generate_random_points_on_surface(self, number_of_points):
        return generate_random_point_cloud2(self.start.x, self.corner_xy.x,
                                            self.start.y, self.corner_xy.y,
                                            self.start.z, self.start.z,
                                            number_of_points)

    def graph(self):
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        xdata = [self.start.x, self.corner_y.x, self.corner_xy.x, self.corner_x.x, self.start.x]
        ydata = [self.start.y, self.corner_y.y, self.corner_xy.y, self.corner_x.y, self.start.y]
        zdata = [self.start.z, self.corner_y.z, self.corner_xy.z, self.corner_x.z, self.start.z]

        ax.plot3D(xdata, ydata, zdata)


# plaska pozioma powierzchnia
#  corner_z ------------ corner_zx
#    |                      |
#   height       ZX         |
#    |                      |
#  start -------width----- corner_x
class SurfaceVertical:
    def __init__(self, start: Point, width, height):
        self.start = start
        self.width = width
        self.height = height

        self.corner_x = Point(start.x + width, start.y, start.z)
        self.corner_z = Point(start.x, start.y, start.z + height)
        self.corner_zx = Point(start.x + width, start.y, start.z + height)

    def generate_random_points_on_surface(self, number_of_points):
        return generate_random_point_cloud2(self.start.x, self.corner_x.x,
                                            self.start.y, self.start.y,
                                            self.start.z, self.corner_zx.z,
                                            number_of_points)

    def graph(self):
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        xdata = [self.start.x, self.corner_z.x, self.corner_zx.x, self.corner_x.x, self.start.x]
        ydata = [self.start.y, self.corner_z.y, self.corner_zx.y, self.corner_x.y, self.start.y]
        zdata = [self.start.z, self.corner_z.z, self.corner_zx.z, self.corner_x.z, self.start.z]

        ax.plot3D(xdata, ydata, zdata)


class SurfaceCylinder:
    def __init__(self, center: Point, radius, height):
        self.center = center
        self.radius = radius
        self.height = height

    def generate_random_points_on_surface(self, number_of_points):
        points = []
        for _ in range(number_of_points):
            rand_h = random.uniform(self.center.z, self.center.z + self.height)
            rand_angle = random.uniform(0, 2 * math.pi)

            x = self.center.x + self.radius * math.sin(rand_angle)
            y = self.center.y + self.radius * math.cos(rand_angle)
            z = rand_h

            rand_point = Point(x, y, z)
            points.append(rand_point)

        return points


def graph_point_array(points):
    xdata = []
    ydata = []
    zdata = []

    for i in points:
        xdata.append(i.x)
        ydata.append(i.y)
        zdata.append(i.z)

    ax = plt.axes(projection='3d')
    ax.scatter3D(xdata, ydata, zdata)


def save_points_to_csv(filename: str, points_array):
    with open(filename, 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = csv.writer(csvfile)
        for p in points_array:
            csvwriter.writerow([p.x, p.y, p.z])


if __name__ == '__main__':
    s1 = Surface(Point(10, 10, 0), Point(20, 20, 0), Point(15, 30, 0))
    point_cloud_s = s1.generate_random_points_on_surface(1000)

    surface_90deg = rotate_point_cloud_x(point_cloud_s, 90)

    fig = plt.figure()

    s1.graph()
    graph_point_array(point_cloud_s + surface_90deg)
    plt.show()

    s2 = SurfaceHorizontal(Point(10, 0, 0), 10, 10)
    # s2.graph()

    point_cloud = s2.generate_random_points_on_surface(500)
    sv = SurfaceVertical(Point(-10, 0, 0), 10, 10)

    point_cloud2 = sv.generate_random_points_on_surface(1000)

    # point_cloud3 = []
    # for p in point_cloud2:
    #     axis = Point(0, 1, 0)  # Axis of rotation
    #     angle = 45
    #     point_cloud3.append(rotate_point_yaxis(p, axis, angle))
    #
    # graph_point_array(point_cloud3)
    # sv.graph()

    sc = SurfaceCylinder(Point(10, -10, 0), 5, 10)
    point_cloud_cylinder = sc.generate_random_points_on_surface(1000)
    graph_point_array(point_cloud_cylinder)

    # graph_point_array(point_cloud_s + point_cloud + point_cloud2 + point_cloud_cylinder)
    plt.show()
    # print(s1)

    save_points_to_csv("Point_data.xyz", point_cloud2)

    print("end")

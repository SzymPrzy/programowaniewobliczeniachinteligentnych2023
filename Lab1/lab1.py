import csv
import math
import random

import matplotlib.pyplot as plt


class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


def generate_rectangular_random_point_cloud(x1, x2, y1, y2, z1, z2, number_of_points):
    points = []
    for _ in range(number_of_points):
        rand_x = random.uniform(x1, x2)
        rand_y = random.uniform(y1, y2)
        rand_z = random.uniform(z1, z2)

        points.append(Point(rand_x, rand_y, rand_z))

    return points


def save_points_to_csv(filename: str, points_array):
    with open(filename, 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["X", "Y", "Z"])
        for p in points_array:
            csvwriter.writerow([p.x, p.y, p.z])


def graph_point_array(ax, points):
    xdata = []
    ydata = []
    zdata = []

    for i in points:
        xdata.append(i.x)
        ydata.append(i.y)
        zdata.append(i.z)

    ax.set_xlabel('$X$', fontsize=20)
    ax.set_ylabel('$Y$', fontsize=20)
    ax.set_zlabel(r'$Z$', fontsize=20)
    # ax.yaxis._axinfo['label']['space_factor'] = 3.0

    ax.scatter3D(xdata, ydata, zdata)


def rotate_point_x(point: Point, center: Point, angle):
    # Translate the coordinate system so that the center point is at the origin
    translated_point = Point(point.x - center.x, point.y - center.y, point.z - center.z)

    # Calculate the sin and cos of the rotation angle
    cos_theta = math.cos(angle)
    sin_theta = math.sin(angle)

    # Apply the 3D rotation matrix around the X-axis to the translated point
    rotated_point = Point(translated_point.x,
                          translated_point.y * cos_theta + translated_point.z * sin_theta,
                          -translated_point.y * sin_theta + translated_point.z * cos_theta)

    # Translate the coordinate system back to its original position
    final_point = Point(rotated_point.x + center.x, rotated_point.y + center.y, rotated_point.z + center.z)

    return final_point


def rotate_point_z(point: Point, center: Point, angle):
    # Translate the coordinate system so that the center point is at the origin
    translated_point = Point(point.x - center.x, point.y - center.y, point.z - center.z)

    # Calculate the sin and cos of the rotation angle
    cos_theta = math.cos(angle)
    sin_theta = math.sin(angle)

    # Apply the 3D rotation matrix around the Z-axis to the translated point
    rotated_point = Point(translated_point.x * cos_theta - translated_point.y * sin_theta,
                          translated_point.x * sin_theta + translated_point.y * cos_theta,
                          translated_point.z)

    # Translate the coordinate system back to its original position
    final_point = Point(rotated_point.x + center.x, rotated_point.y + center.y, rotated_point.z + center.z)

    return final_point


def generate_reference_points(size):
    return [Point(size, size, size), Point(-size, -size, -size), Point(size, size, -size), Point(size, -size, size),
            Point(-size, size, size),
            Point(-size, -size, size), Point(size, -size, -size), Point(-size, size, -size)]


if __name__ == '__main__':
    point = Point(0, 1, 0)
    center = Point(0, 0, 0)
    angle = math.pi * 2

    rotated_point_z = rotate_point_z(point, center, angle)

    print(rotated_point_z.x, rotated_point_z.y, rotated_point_z.z)

    points = [point, rotated_point_z]
    ax = plt.axes(projection='3d')
    graph_point_array(ax, points)
    plt.show()

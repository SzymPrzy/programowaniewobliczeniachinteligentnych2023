from Lab1.lab1 import *


# c. powierzchni cylindrycznej o zadanym promieniu i ograniczonej wysokości

def generate_cylinder(center: Point, radius: float, height: float, number_of_points: int):
    points = []
    for _ in range(number_of_points):
        rand_h = random.uniform(center.z, center.z + height)
        rand_angle = random.uniform(0, 2 * math.pi)

        x = center.x + radius * math.sin(rand_angle)
        y = center.y + radius * math.cos(rand_angle)
        z = rand_h

        rand_point = Point(x, y, z)
        points.append(rand_point)

    return points


if __name__ == '__main__':
    s = 10
    reference_points = [Point(s, s, s), Point(-s, -s, -s), Point(s, s, -s), Point(s, -s, s), Point(-s, s, s),
                        Point(-s, -s, s), Point(s, -s, -s), Point(-s, s, -s)]

    surface_0 = generate_cylinder(Point(-5, 0, 0), 2, 5, 1000)
    surface_1 = generate_cylinder(Point(0, 5, -3), 3, 5, 1000)
    surface_2 = generate_cylinder(Point(-0, 0, 5), 4, 5, 1000)

    ax = plt.axes(projection='3d')
    graph_point_array(ax, reference_points)
    graph_point_array(ax, surface_0)
    graph_point_array(ax, surface_1)
    graph_point_array(ax, surface_2)

    plt.show()

    save_points_to_csv("cylindrical_surfaces.xyz", surface_0)

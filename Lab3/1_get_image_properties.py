import numpy as np
from PIL import Image, ImageOps
from skimage.feature import graycomatrix, graycoprops
from matplotlib import pyplot as plt
import os
import pandas as pd

SLICED_PATH = "sliced_images"


def get_image_properties(folder, image_filename):
    path_to_image = f"{folder}/{image_filename}"
    im = Image.open(path_to_image)

    # print(im.format)
    # print(im.size)

    # plt.imshow(gray_image)
    # plt.show()

    gray_image = im.convert("L")
    reduced_gray_image = gray_image.point(
        lambda x: int(x / 4))  # reduce 254 color depth to 64 color depth by dividing and rounding

    # plt.imshow(reduced_gray_image, cmap=plt.get_cmap('gray'))
    # plt.show()

    pix = np.array(reduced_gray_image)
    distances = [1, 3, 5]
    angles = [0, np.pi / 4, np.pi / 2, (3 / 4) * np.pi]

    glcm = graycomatrix(pix, distances=distances, angles=angles, levels=64,
                        symmetric=True)
    df = pd.DataFrame()
    # dissimilarity, correlation, contrast, energy, homogeneity, ASM
    dissimilarity = graycoprops(glcm, 'dissimilarity')
    correlation = graycoprops(glcm, 'correlation')
    contrast = graycoprops(glcm, 'contrast')
    energy = graycoprops(glcm, 'energy')
    homogeneity = graycoprops(glcm, 'homogeneity')
    ASM = graycoprops(glcm, 'ASM')

    # print(
    #     f"dissimilarity; {dissimilarity}, \n\n correlation:{correlation}, \n\n contrast:{contrast},\n\n energy:{energy},\n\n homogeneity;{homogeneity},\n\n ASM:{ASM} ")

    # for d_idx in range(len(distances)):
    #     for a_idx in range(len(angles)):
    new_row = pd.Series(
        {'texture_name': folder.split("/")[-1],
         'folder_path': folder, 'image_filename': image_filename,

         'distance': distances,
         'angle': angles,

         'dissimilarity': dissimilarity,
         'correlation': correlation,
         'contrast': contrast,
         'energy': energy,
         'homogeneity': homogeneity,
         'ASM': ASM
         })
    # print(new_row)

    df = pd.concat([df, new_row.to_frame().T], ignore_index=True)

    print(df)
    return df


if __name__ == '__main__':

    images_properties = pd.DataFrame()

    for folder_path in os.listdir(SLICED_PATH):
        folder = f"{SLICED_PATH}/{folder_path}"
        print(folder)
        for different_textures_path in os.listdir(folder):
            texture_df = get_image_properties(folder, different_textures_path)
            images_properties = pd.concat([images_properties, texture_df], ignore_index=True)

    print(images_properties)
    images_properties.to_csv("image_properties.csv")
    images_properties.to_pickle("image_properties.pkl")

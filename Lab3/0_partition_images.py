from PIL import Image
from matplotlib import pyplot
import os

BASE_DIR_PATH = "base_images"
SLICED_PATH = "sliced_images"

if __name__ == '__main__':
    tile_size = 128  # remember to clear sliced image folder if you are increasing tile size

    for filename in os.listdir(BASE_DIR_PATH):
        path = f"{BASE_DIR_PATH}/{filename}"
        print(path)

        im = Image.open(path)
        print(im.format)
        print(im.size)
        horizontal_tiles = int((im.size[0] / tile_size) - 1)
        vertical_tiles = int((im.size[1] / tile_size) - 1)
        print(f"Tiles h:{horizontal_tiles} v:{vertical_tiles}")

        dir_name = filename.split(".")[0]
        sliced_path = os.path.join(SLICED_PATH, dir_name)
        print(sliced_path)
        try:
            os.mkdir(sliced_path)
        except FileExistsError:
            pass
        except FileNotFoundError:
            os.mkdir('sliced_images')
            os.mkdir(sliced_path)

        for v in range(vertical_tiles):
            for h in range(horizontal_tiles):
                im1 = im.crop((h * tile_size, v * tile_size, (h + 1) * tile_size, (v + 1) * tile_size))
                # pyplot.imshow(im1)
                # pyplot.show()
                im1.save(f"{sliced_path}/{dir_name}_{v}x{h}.jpg")

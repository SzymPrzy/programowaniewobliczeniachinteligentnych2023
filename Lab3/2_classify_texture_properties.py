import random

from matplotlib import pyplot as plt
import os
import pandas as pd
from sklearn import svm
import numpy as np

if __name__ == '__main__':
    texture_properties = pd.read_pickle("image_properties.pkl")

    different_textures = texture_properties['texture_name'].unique()

    values = []
    labes = []

    for texture_name in different_textures:
        texture = texture_properties[texture_properties["texture_name"] == texture_name]
        texture_prop = texture[['dissimilarity', 'correlation', 'contrast', 'energy', 'homogeneity', 'ASM']]
        # print(texture_prop)
        for data in texture_prop.values:
            flattened = np.hstack(data)
            flattened = flattened.flatten()
            values.append(flattened)
            labes.append(texture_name)

    # run multiple tests for more average value
    training_part = 0.9
    number_of_training_data = len(values)
    number_of_trainings = 5

    stat_correct = 0
    stat_wrong = 0

    for _ in range(number_of_trainings):
        random_idxs = random.sample(range(number_of_training_data), int(number_of_training_data * training_part))

        training_values = []
        training_labels = []
        test_values = []
        test_labels = []
        for i in range(number_of_training_data):
            if i in random_idxs:
                training_values.append(values[i])
                training_labels.append(labes[i])
            else:
                test_values.append(values[i])
                test_labels.append(labes[i])

        clf = svm.SVC()
        clf.fit(training_values, training_labels)

        # test model
        correct = 0
        wrong = 0
        for i in range(len(test_values)):
            result = clf.predict([test_values[i]])
            if result == test_labels[i]:
                correct = correct + 1
            else:
                wrong = wrong + 1

        print(f"Correct predictions:{correct}, wrong predictions:{wrong}, {correct / (correct + wrong)}")

        stat_correct = stat_correct + correct
        stat_wrong = stat_wrong + wrong

    print(
        f"Average: correct predictions:{stat_correct},"
        f" wrong predictions:{stat_wrong}, {stat_correct / (stat_correct + stat_wrong)}")

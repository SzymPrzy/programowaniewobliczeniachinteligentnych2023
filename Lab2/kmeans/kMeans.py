import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

if __name__ == "__main__":
    # Read the point cloud data
    point_cloud = pd.read_csv("../RANSAC/test_surfaces.xyz", delimiter=",")
    ax = plt.axes(projection='3d')
    ax.set_xlabel('$X$', fontsize=20)
    ax.set_ylabel('$Y$', fontsize=20)
    ax.set_zlabel(r'$Z$', fontsize=20)

    ax.scatter3D(point_cloud.X, point_cloud.Y, point_cloud.Z)
    plt.show()

    clusterer = KMeans(n_clusters=3, n_init=10)
    clusters = clusterer.fit_predict(point_cloud)

    point_cloud["Cluster"] = clusters

    print(point_cloud)

    cluster0 = point_cloud[point_cloud["Cluster"] == 0]
    cluster1 = point_cloud[point_cloud["Cluster"] == 1]
    cluster2 = point_cloud[point_cloud["Cluster"] == 2]

    ax = plt.axes(projection='3d')
    ax.set_xlabel('$X$', fontsize=20)
    ax.set_ylabel('$Y$', fontsize=20)
    ax.set_zlabel(r'$Z$', fontsize=20)

    ax.scatter3D(cluster0.X, cluster0.Y, cluster0.Z)
    ax.scatter3D(cluster1.X, cluster1.Y, cluster1.Z)
    ax.scatter3D(cluster2.X, cluster2.Y, cluster2.Z)

    plt.show()

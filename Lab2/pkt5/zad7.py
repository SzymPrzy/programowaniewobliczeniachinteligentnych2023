import pyransac3d as pyrsc

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

if __name__ == '__main__':
    columns = ["X", "Y", "Z", "SA", "SB", "SC"]
    spartial_data = pd.read_csv("conferenceRoom_1.txt", header=None, names=columns,
                                delimiter=" ")  # Load your point cloud as a numpy array (N, 3)

    points = spartial_data[["X", "Y", "Z"]]

    for iteration in range(10):
        points_arr = points.to_numpy()
        print(f"Ponts to clasify:{len(points)}")
        if len(points) < 10:
            print(f"Finished in {iteration}")
            break

        plane1 = pyrsc.Plane()
        best_eq, best_inliers_idx = plane1.fit(points_arr, 0.05, maxIteration=50)

        print(f"best equation: {best_eq}")
        print(f"Best inliers: {best_inliers_idx}")

        best_inlier = points.iloc[best_inliers_idx]
        best_inlier.to_csv(f"extrached_surfaces/surface{iteration}.csv", index=False)
        points = points.drop(best_inliers_idx)
        points = points.reset_index(drop=True)

        # print(points)

        # ax = plt.axes(projection='3d')
        # ax.set_xlabel('$X$', fontsize=20)
        # ax.set_ylabel('$Y$', fontsize=20)
        # ax.set_zlabel(r'$Z$', fontsize=20)
        # scatter_plot_inliers = ax.scatter3D(best_inlier.X, best_inlier.Y, best_inlier.Z)
        # plt.colorbar(scatter_plot_inliers)
        # plt.show()

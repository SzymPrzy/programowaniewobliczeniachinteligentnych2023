import random

from matplotlib import pyplot as plt

from Lab1.lab1 import Point, generate_reference_points, graph_point_array
from Lab1.lab1_a import generate_surface_horizontal
from Lab1.lab1_b import generate_surface_vertical
from Lab1.lab1_c import generate_cylinder

# sys.path.append("Lab1")
ref_points = generate_reference_points(15)


def generate_test_surfaces():
    surface_h = generate_surface_horizontal(Point(10, 7, 0), 5, 5, 1000)
    surface_v = generate_surface_vertical(Point(0, 0, 0), 10, 5, 0, 1000)
    surface_c = generate_cylinder(Point(-5, 0, 0), 2, 5, 1000)

    return surface_c + surface_v + surface_h


def ransac_horizontal_surface(points, iterations, permissible_error):
    for _ in range(iterations):
        # select 4 different random points one the same Z
        points_pool = points
        random_point_on_same_z = []
        while True:
            idx = random.randint(0, len(points_pool))
            random_point = points_pool[idx]

            if len(random_point_on_same_z) == 0:
                # get first point
                random_point_on_same_z.append(random_point)
                points_pool.pop(idx)
            else:
                # find point with the same Z
                if abs(random_point.z - random_point_on_same_z[0].z) < permissible_error:
                    random_point_on_same_z.append(random_point)
                    points_pool.pop(idx)
            if len(random_point_on_same_z) >= 4:
                break

        print(random_point_on_same_z)
        ax = plt.axes(projection='3d')
        graph_point_array(ax, random_point_on_same_z + ref_points)
        plt.show()

        # count inliers and outliers


if __name__ == '__main__':
    surfaces = generate_test_surfaces()


    ax = plt.axes(projection='3d')
    # graph_point_array(ax, surfaces + ref_points)
    # plt.show()

    ransac_horizontal_surface(surfaces, 100, 0.1)

"""
3D RANSAC_Surface implementation in python
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

color_map = plt.get_cmap('Reds')
color_map_inliers = plt.get_cmap('Greens_r')

import random
import math
from numpy.random import default_rng

rng = default_rng()


# https://medium.com/@ajithraj_gangadharan/3d-ransac-algorithm-for-lidar-pcd-segmentation-315d2a51351
class RANSAC_Surface:
    """
    RANSAC_Surface Class
    """

    def __init__(self, points, max_iterations, distance_ratio_threshold):
        self.point_cloud = points
        self.max_iterations = max_iterations
        self.distance_ratio_threshold = distance_ratio_threshold
        self.number_of_points = len(self.point_cloud)

    def run(self):
        """
        method for running the class directly
        :return:
        """
        inliers, outliers, normal_of_the_plane = self._ransac_algorithm(self.max_iterations,
                                                                        self.distance_ratio_threshold)

        return inliers, outliers, normal_of_the_plane

    def _visualize_point_cloud(self):
        """
        Visualize the 3D data
        :return: None
        """
        # Visualize the point cloud data
        ax = plt.axes(projection='3d')
        ax.scatter3D(self.point_cloud.X, self.point_cloud.Y, self.point_cloud.Z)
        plt.show()

    def _ransac_algorithm(self, max_iterations, distance_ratio_threshold):
        """
        Implementation of the RANSAC_Surface logic
        :return: inliers(Dataframe), outliers(Dataframe)
        """

        inliers_result = set()
        result_distances = None
        normal_of_the_plane = None
        while max_iterations:
            max_iterations -= 1
            # print(f"Iteration left:{max_iterations}")

            inliers = rng.permutation(range(len(self.point_cloud.X)))[:3].tolist()

            # print(inliers)

            x1, y1, z1 = self.point_cloud.iloc[inliers[0]][["X", "Y", "Z"]]
            x2, y2, z2 = self.point_cloud.iloc[inliers[1]][["X", "Y", "Z"]]
            x3, y3, z3 = self.point_cloud.iloc[inliers[2]][["X", "Y", "Z"]]

            # Plane Equation --> ax + by + cz + d = 0
            # Value of Constants for inlier plane
            a = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1)
            b = (z2 - z1) * (x3 - x1) - (x2 - x1) * (z3 - z1)
            c = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)
            d = -(a * x1 + b * y1 + c * z1)

            plane_lenght = max(0.1, math.sqrt(a * a + b * b + c * c))

            distances = np.empty((self.number_of_points))

            for index, point in self.point_cloud.iterrows():
                # Skip iteration if point matches the randomly generated inlier point
                if index in inliers:
                    distances[index] = 0
                    continue

                x, y, z = point[["X", "Y", "Z"]]

                # Calculate the distance of the point to the inlier plane
                distance = math.fabs(a * x + b * y + c * z + d) / plane_lenght
                distances[index] = distance
                # Add the point as inlier, if within the threshold distancec ratio
                if distance <= distance_ratio_threshold:
                    inliers.append(index)

            # Update the set for retaining the maximum number of inlier spartial_data
            if len(inliers) > len(inliers_result):
                inliers_result.clear()
                inliers_result = inliers
                result_distances = distances

                v_x = (x1 + x2 + x3) / 3
                v_y = (y1 + y2 + y3) / 3
                v_z = (z1 + z2 + z3) / 3
                normal_of_the_plane = [[v_x, v_y, v_z], [a, b, c, d]]

        # Segregate inliers and outliers from the point cloud
        self.point_cloud["Distances"] = result_distances
        inlier_points = self.point_cloud.iloc[inliers_result]
        outlier_points = self.point_cloud[~self.point_cloud.index.isin(inliers_result)]

        return inlier_points, outlier_points, normal_of_the_plane


class RANSAC_Cylinder:
    """
    RANSAC_Surface Class
    """

    def __init__(self, points, max_iterations, distance_ratio_threshold):
        self.point_cloud = points
        self.max_iterations = max_iterations
        self.distance_ratio_threshold = distance_ratio_threshold
        self.number_of_points = len(self.point_cloud)

    def run(self):
        """
        method for running the class directly
        :return:
        """
        inliers, outliers, circle = self._ransac_algorithm(self.max_iterations,
                                                           self.distance_ratio_threshold)

        return inliers, outliers, circle

    # https://math.stackexchange.com/questions/213658/get-the-equation-of-a-circle-when-given-3-points
    def _circle_from_3_points(self, z1: complex, z2: complex, z3: complex) -> tuple[complex, float]:
        if (z1 == z2) or (z2 == z3) or (z3 == z1):
            raise ValueError(f"Duplicate points: {z1}, {z2}, {z3}")

        w = (z3 - z1) / (z2 - z1)

        # You should change 0 to a small tolerance for floating point comparisons
        if abs(w.imag) <= 0:
            raise ValueError(f"Points are collinear: {z1}, {z2}, {z3}")

        c = (z2 - z1) * (w - abs(w) ** 2) / (2j * w.imag) + z1  # Simplified denominator
        r = abs(z1 - c)

        return c, r

    def calculate_distances(self, point, c, r):
        # print(f"{point}, {c}, {r}")
        cy = c.imag
        cx = c.real

        px = point[0]
        py = point[1]

        return abs(math.dist([cx, cy], [px, py]) - r)

    def _visualize_point_cloud(self):
        """
        Visualize the 3D data
        :return: None
        """
        # Visualize the point cloud data
        ax = plt.axes(projection='3d')
        ax.scatter3D(self.point_cloud.X, self.point_cloud.Y, self.point_cloud.Z)
        plt.show()

    def _ransac_algorithm(self, max_iterations, distance_ratio_threshold):
        """
        Implementation of the RANSAC_Surface logic
        :return: inliers(Dataframe), outliers(Dataframe)
        """

        inliers_result = set()
        result_distances = None
        circle = None
        while max_iterations:
            max_iterations -= 1

            while True:
                try:
                    inliers = rng.permutation(range(len(self.point_cloud.X)))[:3].tolist()
                    x1, y1, z1 = self.point_cloud.iloc[inliers[0]][["X", "Y", "Z"]]
                    x2, y2, z2 = self.point_cloud.iloc[inliers[1]][["X", "Y", "Z"]]
                    x3, y3, z3 = self.point_cloud.iloc[inliers[2]][["X", "Y", "Z"]]
                    c, r = self._circle_from_3_points(complex(x1, y1), complex(x2, y2), complex(x3, y3))
                    break
                except:
                    continue

            distances = np.empty((self.number_of_points))

            for index, point in self.point_cloud.iterrows():
                # Skip iteration if point matches the randomly generated inlier point
                if index in inliers:
                    distances[index] = 0
                    continue

                x, y, z = point[["X", "Y", "Z"]]

                # Calculate the distance of the point to the inlier plane
                distance = self.calculate_distances([x, y, z], c, r)
                distances[index] = distance
                # Add the point as inlier, if within the threshold distancec ratio
                if distance <= distance_ratio_threshold:
                    inliers.append(index)

            # Update the set for retaining the maximum number of inlier spartial_data
            if len(inliers) > len(inliers_result):
                inliers_result.clear()
                inliers_result = inliers
                result_distances = distances
                circle = [c, r]

        # Segregate inliers and outliers from the point cloud
        self.point_cloud["Distances"] = result_distances
        inlier_points = self.point_cloud.iloc[inliers_result]
        outlier_points = self.point_cloud[~self.point_cloud.index.isin(inliers_result)]

        return inlier_points, outlier_points, circle


def plot_inliers_outliers(inliers, outliers):
    global ax, scatter_plot_inliers, scatter_plot_outliers
    ax = plt.axes(projection='3d')
    ax.set_xlabel('$X$', fontsize=20)
    ax.set_ylabel('$Y$', fontsize=20)
    ax.set_zlabel(r'$Z$', fontsize=20)
    scatter_plot_inliers = ax.scatter3D(inliers.X, inliers.Y, inliers.Z, c=inliers.Distances,
                                        cmap=color_map_inliers)
    scatter_plot_outliers = ax.scatter3D(outliers.X, outliers.Y, outliers.Z, c=outliers.Distances,
                                         cmap=color_map)
    plt.colorbar(scatter_plot_outliers)
    plt.colorbar(scatter_plot_inliers)

    return ax


if __name__ == "__main__":
    # Read the point cloud data
    point_cloud = pd.read_csv("test_surfaces.xyz", delimiter=",")

    # Run RANSAC_Cylinder
    APPLICATION_cylinder = RANSAC_Cylinder(point_cloud, max_iterations=50, distance_ratio_threshold=0.01)
    inliers_c, outliers_c, circle = APPLICATION_cylinder.run()
    print(f"Circle center:{circle[0]} radius:{circle[1]}")
    plot_inliers_outliers(inliers_c, outliers_c)
    plt.show()

    ds2 = set(map(tuple, outliers_c[["X", "Y", "Z"]].values))
    point_cloud = pd.DataFrame(list(ds2), columns=["X", "Y", "Z"])

    # Run RANSAC_Surface
    APPLICATION = RANSAC_Surface(point_cloud, max_iterations=50, distance_ratio_threshold=0.01)
    inliers, outliers, normal = APPLICATION.run()

    print(f"Normal of the plane:{normal}")

    ax = plot_inliers_outliers(inliers, outliers)
    ax.quiver(normal[0][0], normal[0][1], normal[0][2], normal[1][0], normal[1][1], normal[1][2])
    plt.show()

    ds2 = set(map(tuple, outliers[["X", "Y", "Z"]].values))
    new_points = pd.DataFrame(list(ds2), columns=["X", "Y", "Z"])

    APPLICATION2 = RANSAC_Surface(new_points, max_iterations=50, distance_ratio_threshold=0.1)
    APPLICATION2._visualize_point_cloud()
    inliers2, outliers2, normal2 = APPLICATION2.run()

    print(f"Normal of the plane:{normal2}")

    ax = plot_inliers_outliers(inliers2, outliers2)
    ax.quiver(normal2[0][0], normal2[0][1], normal2[0][2], normal2[1][0], normal2[1][1], normal2[1][2])
    plt.show()

    print("end")

from matplotlib import pyplot as plt

from Lab1.lab1 import Point, generate_reference_points, graph_point_array, save_points_to_csv
from Lab1.lab1_a import generate_surface_horizontal
from Lab1.lab1_b import generate_surface_vertical
from Lab1.lab1_c import generate_cylinder

ref_points = generate_reference_points(15)


def generate_test_surfaces():
    surface_h = generate_surface_horizontal(Point(10, 7, 0), 5, 5, 200)
    surface_v = generate_surface_vertical(Point(0, 0, 0), 10, 5, 0, 200)
    surface_c = generate_cylinder(Point(-5, 0, 0), 2, 5, 200)

    return surface_c + surface_v + surface_h


if __name__ == '__main__':
    surfaces = generate_test_surfaces()

    ax = plt.axes(projection='3d')
    graph_point_array(ax, surfaces + ref_points)
    plt.show()

    save_points_to_csv("test_surfaces.xyz", surfaces)
